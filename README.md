# MathaQuilta

A client-side web app based on the [Mathaquilta iOS App by Michael Rogers](https://itunes.apple.com/ee/app/mathaquilta/id1029232907?mt=8).

## Contacts

- Mary Shepherd, msheprd@nwmissouri.edu
- Denise Case, dcase@nwmissouri.edu

## Technologies

- **Chrome**, a browser for accessing the world wide web and running web apps.
- **HTML**, a markup language for web apps
- **CSS**, a style sheet language for styling markup languages such as HTML
- **JavaScript**, a scripting language for the web
- **jQuery**, a JavaScript library that simplifies our code
- **BootStrap**, a framework for responsive web design that adjusts the app automatically to fit the screen size
- **BootStrap navbar**, for the responsive menu
- **Git**, a distributed version control system to manage evolving code
- **TortoiseGit**, a code management app that integrates Git with Windows File Explorer
- **Visual Studio Code**, a light-weight, easy-to-use text editor and development environment for working with code

## Initial Setup

We need to do a few things first just to get the computer ready to run and edit the app. See a short description in the previous section.

1. Create an account on [BitBucket.org](https://bitbucket.org/).
2. Install [Git for Windows](https://git-scm.com/download/win).
3. Install [TortoiseGit](https://tortoisegit.org/download/) for 64-bit Windows.
4. Install [Visual Studio Code](https://code.visualstudio.com/) - BE SURE TO CHECK ALL CHECKBOXES DURING INSTALLATION. If you forget, you can always reinstall.
 
Visual Studio Code is recommended, but you can use Sublime, Notepad++, or any text editor to modify the code.

## Set up VS Code

Open Visual Studio Code. Hit the Windows key and type "Visual Studio Code" until it appears and then click on it to open.

Click 'File' on the main menu. Make sure the 'Autosave' option has a check beside it. If not, click on it to turn this option on. Now, all changes will be saved automagically.

## Get The Code

1. Create a folder for your apps. If you don't have one, create a folder named 'git'.
2. Copy the following link to your clipboard (select it and hit CTRL and C):
3. In Windows File Explorer, right-click on the git folder and select: Git clone...
4. Click OK. (The URL field should fill in automatically, if not, type or paste the link into the URL field).

This will bring a copy of the code down to your machine for running and editing.

## Open the App

- In the mathaquilta folder, right-click on index.html and open with Chrome.

## Edit The Code

- Right-click on the mathaquilta folder and select "Open with Code".
- When you're done making changes, in VS Code, format the code with SHIFT + ALT + f.
- In the mathaquilta folder, right-click on index.html and open with Chrome.
- If you app is already open in Chrome, just hit the browser 'Refresh' button to view your updated app.

## Resources

- Dillinger.io (to validate markdown like this file): http://dillinger.io/
- W3C Markup Validation Service: https://validator.w3.org/

## Preview

![Mathaquilta](images/Index.PNG)

## Snake Trail Designer

![Snake Trail Designer](images/SnakeTrail.PNG)

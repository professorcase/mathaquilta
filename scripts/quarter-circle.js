var module = (function ($) {

  // Global variables

  let foregroundColor = "purple";
  let backgroundColor = "crean";

  const DEFAULT_ROWS = 5;
  const DEFAULT_COLS = 4;

  let nRows = DEFAULT_ROWS;
  let nCols = DEFAULT_COLS;
  let isRandom = false;

  // Initialization

  document.getElementById("numrows").value = DEFAULT_ROWS;
  document.getElementById("numcolumns").value = DEFAULT_COLS;
  document.getElementById("quarter-circle-quilt").hidden = true;

  // Configure Event Handlers 

  $('#foreground').colorpicker({ "color": "#ff00ff" })
    .on('colorpickerChange colorpickerCreate', function (e) {
      foregroundColor = e.color;
      drawQuarterCircles();
    });

  $('#background').colorpicker({ "color": "#ffffcc" })
    .on('colorpickerChange colorpickerCreate', function (e) {
      backgroundColor = e.color;
      drawQuarterCircles();
    });

  document.getElementById("numrows").addEventListener('change', function () {
    if (document.getElementById("numrows").value > 1) {
      nRows = document.getElementById("numrows").value;
    }
    else { nRows = DEFAULT_ROWS; }
    drawQuarterCirclesQuilt();
  });

  document.getElementById("numcolumns").addEventListener('change', function () {
    if (document.getElementById("numcolumns").value > 1) {
      nCols = document.getElementById("numcolumns").value;
    }
    else { nCols = DEFAULT_COLS; }
    drawQuarterCirclesQuilt();
  });

  document.getElementById("is-randomized").addEventListener('change', function () {
    if (document.getElementById("is-randomized").checked) {
      isRandom = true;
    }
    else { isRandom = false; }
    drawQuarterCirclesQuilt();
  });

  document.getElementById("btnDrawQuarterCirclesQuilt").addEventListener('click', function () {
    drawQuarterCirclesQuilt();
  });

  function drawQuarterCircles() {
    const canvas = document.getElementById("quarter-circle-square");
    const context = canvas.getContext("2d");
    context.fillStyle = backgroundColor;
    context.fillRect(0, 0, canvas.width, canvas.height); // full size
    const x1 = 0;
    const y1 = 0;
    const x2 = canvas.width;
    const y2 = canvas.height;
    const outerRadius = canvas.width * 0.7917;
    // const innerRadius = 0;
    drawNWM(context, x1, y1, x2, y2, outerRadius);
    return true;
  }

  function drawQuarterCirclesQuilt() {
    document.getElementById("quarter-circle-quilt").hidden = false;
    nRows = document.getElementById("numrows").value;
    nCols = document.getElementById("numcolumns").value;
    const squareWidth = document.getElementById("quarter-circle-square").width;
    const squareHeight = document.getElementById("quarter-circle-square").height;
    const quiltWidth = squareWidth * nCols;
    const quiltHeight = squareHeight * nRows;
    document.getElementById("quarter-circle-quilt").width = quiltWidth;
    document.getElementById("quarter-circle-quilt").height = quiltHeight;
    const canvas = document.getElementById("quarter-circle-quilt");
    const context = canvas.getContext("2d");
    context.fillStyle = backgroundColor;
    context.fillRect(0, 0, canvas.width, canvas.height); // full 
    for (let iRow = 0; iRow < nRows; iRow++) {
      for (let iCol = 0; iCol < nCols; iCol++) {
        const x1 = 0 + iCol * squareWidth;
        const y1 = 0 + iRow * squareHeight;
        const x2 = x1 + squareWidth;
        const y2 = y1 + squareHeight;
        const outerRadius = squareWidth * 0.7917;
        // const innerRadius = 0;
        // if random, and NESW, send in the NE and SW points instead
        if (isRandom) {
          /*  
          ((Math.random() < 0.25) ?
             drawNWM(context, x1, y1, x2, y2, outerRadius)
             : 
            
             (Math.random() < .5) ?
             drawNEM(context, x1 + squareWidth, y1, x2 - squareWidth, y2, outerRadius)
             : (Math.random() < .75) ?
             drawSEM(context, x1 + squareWidth, y1 + squareHeight, x2 - squareWidth, y2 - squareHeight, outerRadius)
             :
             

             drawSWM(context, x1 + squareWidth, y1 + squareHeight, x2 - squareWidth, y2 - squareHeight, outerRadius);
             */
          (Math.random() < 0.5) ?
          drawNWM(context, x1, y1, x2, y2, outerRadius)
          : drawNEM(context, x1 + squareWidth, y1, x2 - squareWidth, y2, outerRadius);
        }
        else drawNWM(context, x1, y1, x2, y2, outerRadius);
      }
    }
  }

  // drawing functions

  function drawNWM(context, x1, y1, x2, y2, outerRadius) {
    context.fillStyle = backgroundColor;
    context.fillRect(x1, y1, x2, y2); // full
    drawNW(context, x1, y1, outerRadius);
    
  }

  function drawNW(context, x, y, outerRadius) { 
    const startAngleNW = 0;  // due east of upper left x, y
    const endAngleNW = 0.5 * Math.PI;
    context.beginPath();
    context.moveTo(x, y);
    context.arc(x, y, outerRadius, startAngleNW, endAngleNW);
    context.closePath();
    context.fillStyle = foregroundColor;
    context.fill();
     
  }

  function drawNWC(context, x1, y1, outerRadius) {  //not yet done for NW with colors reversed
    drawNWcolor(context, x1, y1, outerRadius);
    // drawSE(context, x2, y2, outerRadius, innerRadius);
  }

  function drawNWcolor(context, x, y, outerRadius) {  //not yet done for NW with colors reversed
    const startAngleNW = 0;  // due east of upper left x, y
    const endAngleNW = 0.5 * Math.PI;
    context.beginPath();
    context.moveTo(x, y);
    context.arc(x, y, outerRadius, startAngleNW, endAngleNW);
    context.closePath();
    context.fillStyle = foregroundColor;
    context.fill();
    // context.beginPath();
    // context.moveTo(x, y);
    // context.arc(x, y, innerRadius, startAngleNW, endAngleNW);
    // context.closePath();
    // context.fillStyle = backgroundColor;
    //context.fill();
  }

  function drawSEM(context, x1, y1, x2, y2, outerRadius) {
    context.fillStyle = backgroundColor;
    context.fillRect(x1, y1, x2, y2); // full
    drawSE(context, x1, y1, outerRadius);
    
  }

  function drawSE(context, x, y, outerRadius) {
    const startAngleSE = 1.0 * Math.PI; // due west of lower right x, y
    const endAngleSE = 1.5 * Math.PI;
    context.beginPath();
    context.moveTo(x, y);
    context.arc(x, y, outerRadius, startAngleSE, endAngleSE);
    context.closePath();
    context.fillStyle = foregroundColor;
    context.fill();
    // context.beginPath();
    // context.moveTo(x, y);
    // context.arc(x, y, innerRadius, startAngleSE, endAngleSE);
    // context.closePath();
    // context.fillStyle = backgroundColor;
    // context.fill();
  }

  // For use if randomizing or reversing the direction of the arcs

  function drawNEM(context, x1, y1, x2, y2, outerRadius) {
    context.fillStyle = backgroundColor;
    context.fillRect(x1, y1, x2, y2);
    drawNE(context, x1, y1, outerRadius);  
   // drawSW(context, x2, y2, outerRadius, innerRadius);  // not written
  }

  function drawNE(context, x, y, outerRadius) {
    const startAngleNW = 0.5 * Math.PI;  // due south of upper right x, y
    const endAngleNW = 1.0 * Math.PI;
    context.beginPath();
    context.moveTo(x, y);
    context.arc(x, y, outerRadius, startAngleNW, endAngleNW);
    context.closePath();
    context.fillStyle = foregroundColor;
    context.fill();
    // context.beginPath();
    // context.moveTo(x, y);
    // context.arc(x, y, innerRadius, startAngleNW, endAngleNW);
    // context.closePath();
    // context.fillStyle = backgroundColor;
    // context.fill();
  }

  function drawSWM(context, x1, y1, x2, y2, outerRadius) {
    context.fillStyle = backgroundColor;
    context.fillRect(x1, y1, x2, y2);
    drawSW(context, x1, y1, outerRadius);

  }
  
  function drawSW(context, x, y, outerRadius) {
    const startAngleSE = 1.5 * Math.PI; // due north of lower left x, y
    const endAngleSE = 0.0;
    context.beginPath();
    context.moveTo(x, y);
    context.arc(x, y, outerRadius, startAngleSE, endAngleSE);
    context.closePath();
    context.fillStyle = foregroundColor;
    context.fill();
    // context.beginPath();
    // context.moveTo(x, y);
    // context.arc(x, y, innerRadius, startAngleSE, endAngleSE);
    // context.closePath();
    // context.fillStyle = backgroundColor;
    // context.fill();
  }

}(jQuery));



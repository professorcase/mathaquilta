var module = (function ($) {

  // Global variables

  let foregroundColor = "purple";
  let backgroundColor = "cream";

  const DEFAULT_ROWS = 5;
  const DEFAULT_COLS = 4;

  let nRows = DEFAULT_ROWS;
  let nCols = DEFAULT_COLS;
  let isRandom = false;

  // Initialization

  document.getElementById("numrows").value = DEFAULT_ROWS;
  document.getElementById("numcolumns").value = DEFAULT_COLS;
  document.getElementById("snakes-quilt").hidden = true;

  // Configure Event Handlers 

  $('#foreground').colorpicker({ "color": "#CC00CC" })
    .on('colorpickerChange colorpickerCreate', function (e) {
      foregroundColor = e.color;
      drawSnakes();
    });

  $('#background').colorpicker({ "color": "#FFFFCC" })
    .on('colorpickerChange colorpickerCreate', function (e) {
      backgroundColor = e.color;
      drawSnakes();
    });

  document.getElementById("numrows").addEventListener('change', function () {
    if (document.getElementById("numrows").value > 1) {
      nRows = document.getElementById("numrows").value;
    }
    else { nRows = DEFAULT_ROWS; }
    drawSnakesQuilt();
  });

  document.getElementById("numcolumns").addEventListener('change', function () {
    if (document.getElementById("numcolumns").value > 1) {
      nCols = document.getElementById("numcolumns").value;
    }
    else { nCols = DEFAULT_COLS; }
    drawSnakesQuilt();
  });

  document.getElementById("is-randomized").addEventListener('change', function () {
    if (document.getElementById("is-randomized").checked) {
      isRandom = true;
    }
    else { isRandom = false; }
    drawSnakesQuilt();
  });

  document.getElementById("btnDrawSnakesQuilt").addEventListener('click', function () {
    drawSnakesQuilt();
  });

  function drawSnakes() {
    const canvas = document.getElementById("snakes-square");
    const context = canvas.getContext("2d");
    context.fillStyle = backgroundColor;
    context.fillRect(0, 0, canvas.width, canvas.height); // full size
    const x1 = 0;
    const y1 = 0;
    const x2 = canvas.width;
    const y2 = canvas.height;
    const outerRadius = canvas.width / 2.0 * 1.17;
    const innerRadius = canvas.width / 2.0 * 0.83;
    drawNWSE(context, x1, y1, x2, y2, outerRadius, innerRadius);
    return true;
  }

  function drawSnakesQuilt() {
    document.getElementById("snakes-quilt").hidden = false;
    nRows = document.getElementById("numrows").value;
    nCols = document.getElementById("numcolumns").value;
    const squareWidth = document.getElementById("snakes-square").width;
    const squareHeight = document.getElementById("snakes-square").height;
    const quiltWidth = squareWidth * nCols;
    const quiltHeight = squareHeight * nRows;
    document.getElementById("snakes-quilt").width = quiltWidth;
    document.getElementById("snakes-quilt").height = quiltHeight;
    const canvas = document.getElementById("snakes-quilt");
    const context = canvas.getContext("2d");
    context.fillStyle = backgroundColor;
    context.fillRect(0, 0, canvas.width, canvas.height); // full 
    for (let iRow = 0; iRow < nRows; iRow++) {
      for (let iCol = 0; iCol < nCols; iCol++) {
        const x1 = 0 + iCol * squareWidth;
        const y1 = 0 + iRow * squareHeight;
        const x2 = x1 + squareWidth;
        const y2 = y1 + squareHeight;
        const outerRadius = squareWidth / 2.0 * 1.17;
        const innerRadius = squareWidth / 2.0 * 0.83;
        // if random, and NESW, send in the NE and SW points instead
        if (isRandom) {
          (Math.random() < 0.5) ?
            drawNWSE(context, x1, y1, x2, y2, outerRadius, innerRadius)
            : drawNESW(context, x1 + squareWidth, y1, x2 - squareWidth, y2, outerRadius, innerRadius);
        }
        else drawNWSE(context, x1, y1, x2, y2, outerRadius, innerRadius);
      }
    }
  }

  // example of using local storage to save information (taken from another app)
  function rememberClicks() {
    if (localStorage.getItem("clicks")) {
      const value = Number(localStorage.clicks) + 1;
      localStorage.setItem("clicks", value);
    } else {
      localStorage.clicks = 1;
    }
    const s = "You have clicked this button " + localStorage.clicks + " times";
    $("#clicks").html(s); // display forever clicks 
  }

  // drawing functions

  function drawNWSE(context, x1, y1, x2, y2, outerRadius, innerRadius) {
    drawNW(context, x1, y1, outerRadius, innerRadius);
    drawSE(context, x2, y2, outerRadius, innerRadius);
  }

  function drawNW(context, x, y, outerRadius, innerRadius) {
    const startAngleNW = 0;  // due east of upper left x, y
    const endAngleNW = 0.5 * Math.PI;
    context.beginPath();
    context.moveTo(x, y);
    context.arc(x, y, outerRadius, startAngleNW, endAngleNW);
    context.closePath();
    context.fillStyle = foregroundColor;
    context.fill();
    context.beginPath();
    context.moveTo(x, y);
    context.arc(x, y, innerRadius, startAngleNW, endAngleNW);
    context.closePath();
    context.fillStyle = backgroundColor;
    context.fill();
  }

  function drawSE(context, x, y, outerRadius, innerRadius) {
    const startAngleSE = 1.0 * Math.PI; // due west of lower right x, y
    const endAngleSE = 1.5 * Math.PI;
    context.beginPath();
    context.moveTo(x, y);
    context.arc(x, y, outerRadius, startAngleSE, endAngleSE);
    context.closePath();
    context.fillStyle = foregroundColor;
    context.fill();
    context.beginPath();
    context.moveTo(x, y);
    context.arc(x, y, innerRadius, startAngleSE, endAngleSE);
    context.closePath();
    context.fillStyle = backgroundColor;
    context.fill();
  }

  // For use if randomizing or reversing the direction of the arcs

  function drawNESW(context, x1, y1, x2, y2, outerRadius, innerRadius) {
    drawNE(context, x1, y1, outerRadius, innerRadius);  // not written
    drawSW(context, x2, y2, outerRadius, innerRadius);  // not written
  }

  function drawNE(context, x, y, outerRadius, innerRadius) {
    const startAngleNW = 0.5 * Math.PI;  // due south of upper right x, y
    const endAngleNW = 1.0 * Math.PI;
    context.beginPath();
    context.moveTo(x, y);
    context.arc(x, y, outerRadius, startAngleNW, endAngleNW);
    context.closePath();
    context.fillStyle = foregroundColor;
    context.fill();
    context.beginPath();
    context.moveTo(x, y);
    context.arc(x, y, innerRadius, startAngleNW, endAngleNW);
    context.closePath();
    context.fillStyle = backgroundColor;
    context.fill();
  }

  function drawSW(context, x, y, outerRadius, innerRadius) {
    const startAngleSE = 1.5 * Math.PI; // due north of lower left x, y
    const endAngleSE = 0.0;
    context.beginPath();
    context.moveTo(x, y);
    context.arc(x, y, outerRadius, startAngleSE, endAngleSE);
    context.closePath();
    context.fillStyle = foregroundColor;
    context.fill();
    context.beginPath();
    context.moveTo(x, y);
    context.arc(x, y, innerRadius, startAngleSE, endAngleSE);
    context.closePath();
    context.fillStyle = backgroundColor;
    context.fill();
  }

}(jQuery));


